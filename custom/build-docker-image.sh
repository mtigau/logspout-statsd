prev_ver="0.4.11"
curr_ver="0.4.12"

docker rmi -f "mugurax/logspout-statsd:$prev_ver"
docker rmi -f "mugurax/logspout-statsd:$curr_ver"

docker build -t "mugurax/logspout-statsd:$curr_ver" .
docker push "mugurax/logspout-statsd:$curr_ver"