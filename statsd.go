/*
	expects labels so we can build metric names
		- project_code=TT
   	- com.amazonaws.ecs.cluster=DEV
  	- com.amazonaws.ecs.container-name=apisrv

  accepted format is
  	REQ:GET /url... ... ST:2xx ... RT:3.021 or RT_MS:12 ...
	  RT is in seconds (from nginx, ie)
	  RT_MS is in ms (from morgan, ie)

*/

package statsd

import (
	// "errors"
	"fmt"
	"log"
	// "net"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gliderlabs/logspout/router"
	"gopkg.in/alexcesaro/statsd.v2"
)

var (
	Version = "0.4.9"

	ClusterFilter   = os.Getenv("CLUSTER")
	ContainerFilter = os.Getenv("CONTAINER_NAME")

	bCountHTTPCodes = false
	bSendRspTimers  = false
	bTrackAllReqs   = false
)

func init() {
	fmt.Println("init - statsd adapter ver", Version)

	bCountHTTPCodes, _ = strconv.ParseBool(os.Getenv("COUNT_HTTP_RSP_CODES"))
	bSendRspTimers, _ = strconv.ParseBool(os.Getenv("SEND_HTTP_RSP_TIMES"))
	bTrackAllReqs, _ = strconv.ParseBool(os.Getenv("TRACK_ALL_REQS"))

	router.AdapterFactories.Register(NewStatsdAdapter, "statsd")
}

// StatsdAdapter is an adapter that streams UDP info to a Statsd server.
type StatsdAdapter struct {
	statsd *statsd.Client
	route  *router.Route
}

func NewStatsdAdapter(route *router.Route) (router.LogAdapter, error) {

	const attempts = 10

	var statsdClient *statsd.Client
	var err error

	for i := 0; i < attempts; i++ {
		statsdClient, err = statsd.New(statsd.Address(route.Address))
		if err == nil {
			fmt.Println("statsd client ok")
			break
		} else {
			log.Printf("failed to connectd to statsd %v, retrying in 10 sec ...\n", route.Address)
			time.Sleep(10 * time.Second)
		}
	}

	if err != nil {
		log.Fatalln("failed to connectd to statsd, bye.")
	}

	return &StatsdAdapter{
		route:  route,
		statsd: statsdClient,
	}, nil

}

// Stream implements the router.LogAdapter interface.
func (a *StatsdAdapter) Stream(logstream chan *router.Message) {

	for m := range logstream {

		// fmt.Printf("message: %v, source: %v\n", stripColorCodesFromString(m.Data), m.Source)

		if len(ClusterFilter) > 0 {
			if m.Container.Config.Labels["com.amazonaws.ecs.cluster"] != ClusterFilter {
				fmt.Println("skipped msg from cluster", m.Container.Config.Labels["com.amazonaws.ecs.cluster"])
				continue
			}
		}

		if len(ContainerFilter) > 0 && ContainerFilter != "*" {
			if m.Container.Config.Labels["com.amazonaws.ecs.container-name"] != ContainerFilter {
				fmt.Println("skipped msg from container", m.Container.Config.Labels["com.amazonaws.ecs.container-name"])
				continue
			}
		}

		message := stripColorCodesFromString(m.Data)
		var ok bool
		var value uint64
		var rxx string

		var hasMetricName bool
		var metric_name string

		if bTrackAllReqs {
			hasMetricName, metric_name = getRequestPath(message)
		}

		if bCountHTTPCodes || bSendRspTimers {
			ok, value, rxx = get_REQ_ST(message)
		}

		if bCountHTTPCodes && ok {
			a.statsd.Increment("http_" + rxx + "#" + getTagsFromLabels(m.Container.Config.Labels))

			if rxx == "2xx" && bTrackAllReqs {
				if hasMetricName {
					a.statsd.Increment("http_" + rxx + "_" + metric_name + "#" + getTagsFromLabels(m.Container.Config.Labels))
				} else {
					fmt.Println("failed to req_path->metric:", message)
				}
			}
		}

		if bSendRspTimers && ok && rxx == "2xx" {
			ok, value = get_REQ_MS(message)
			if ok {
				// fmt.Printf("get_REQ_MS got rsp:[%v]\n", value)
				a.statsd.Timing("http_2xx_ms"+"#"+getTagsFromLabels(m.Container.Config.Labels), value)

				if hasMetricName {
					a.statsd.Timing("http_2xx_"+metric_name+"_ms#"+getTagsFromLabels(m.Container.Config.Labels), value)
				}

			}
		}

	}
}

func getTagsFromLabels(labels map[string]string) string {

	result := ""

	if len(labels["project_code"]) > 0 {
		if result != "" {
			result += ","
		}
		result += "project_code=" + labels["project_code"]
	}

	if len(labels["com.amazonaws.ecs.cluster"]) > 0 {
		if result != "" {
			result += ","
		}
		result += "cluster=" + labels["com.amazonaws.ecs.cluster"]
	}

	if len(labels["com.amazonaws.ecs.container-name"]) > 0 {
		if result != "" {
			result += ","
		}
		result += "app=" + labels["com.amazonaws.ecs.container-name"]
	}
	return strings.ToLower(result)
}

func get_REQ_ST(msg string) (bool, uint64, string) {
	idxReq := strings.Index(msg, "REQ:")
	if idxReq < 0 {
		return false, 0, ""
	}

	idxStatus := strings.Index(msg, "ST:")
	if idxStatus < 0 {
		return false, 0, ""
	}

	if len(msg) < idxStatus+6 {
		return false, 0, ""
	}

	status := string(msg[idxStatus+3]) + string(msg[idxStatus+4]) + string(msg[idxStatus+5])

	rsp, err := strconv.ParseUint(status, 10, 32)

	if rsp >= 200 && rsp <= 299 && err == nil {
		return true, rsp, "2xx"
	}

	if rsp >= 300 && rsp <= 399 && err == nil {
		return true, rsp, "3xx"
	}

	if rsp >= 400 && rsp <= 499 && err == nil {
		return true, rsp, "4xx"
	}

	if rsp >= 500 && rsp <= 599 && err == nil {
		return true, rsp, "5xx"
	}

	return true, 0, status
}

func stripColorCodesFromString(str string) string {

	var stripping = false

	return strings.Map(func(r rune) rune {
		if r == 0x1b {
			stripping = true
			return -1
		}
		if r == 0x6D && stripping == true {
			stripping = false
			return -1

		}
		if stripping {
			return -1
		}

		return r

	}, str)
}

func get_REQ_MS(msg string) (bool, uint64) {
	log.Println("getrqms=>")

	// look for timing in miliseconds
	idxMs := strings.Index(msg, "MS:")
	if idxMs > -1 {
		log.Println("found ms")
		// found timing in miliseconds
		reg, err := regexp.Compile("[^.0-9]+")
		if err != nil {
			log.Println(err)
			return false, 0
		}
		sMS := reg.ReplaceAllString(msg[idxMs+3:], "")

		rsp, err := strconv.ParseUint(sMS, 10, 32)

		return true, rsp
	}

	log.Println("getrqms=> no ms found, looking for sec")
	//look for timing in seconds
	pos := strings.Index(msg, "RT:")
	if pos < 0 {
		log.Printf("getrqms=> nothing found. msg:[%v], pos:[%v]\n", msg, pos)
		// no timing found :(
		return false, 0
	}
	// log.Printf("getrqms=> found s, pos:[%v]\n", pos)
	// found timing in seconds
	reg, err := regexp.Compile("[^.0-9]+")
	if err != nil {
		log.Println(err)
		return false, 0
	}
	sec := reg.ReplaceAllString(msg[pos+3:], "")
	log.Printf("getrqms=>  sec:[%v]\n", sec)
	rsp, err := strconv.ParseFloat(sec, 10)
	if err != nil {
		log.Println(err)
	}
	// log.Printf("getrqms=>  rsp1:[%v]\n", rsp)
	rsp = rsp * 1000
	// log.Printf("getrqms=>  rsp2:[%v]\n", rsp)

	return true, uint64(rsp)
}

/*
transforms requests to metric names

REQ:GET / ST:302 RT_MS:0
get_

REQ:PUT /v4/devices/4809beda-5b09-4d98-aa6c-0c7abcd01cd1/tango_apps
put_v4_devices_x_tango_apps

REQ:GET /v4/analytics/devices/os_distribution?cid=5943d6c466d63117000e407e ST:200 RT_MS:19
get_v4_analytics_devices_os_distribution
*/
func getRequestPath(msg string) (bool, string) {
	// detect method
	pos := strings.Index(msg, "REQ:")
	if pos != 0 {
		return false, ""
	}

	result := ""

	words := strings.Fields(msg)
	method := words[0][4:]

	result = method

	path := words[1]
	// drop everything after ?
	sep := strings.Index(path, "?")
	if sep > -1 {
		path = path[:strings.Index(path, "?")]
	}

	// detect uid and replace it with "uid"
	tokens := strings.Split(path[1:], "/")
	newPath := ""
	for _, t := range tokens {
		if (len(t) == 36) && (t[8] == '-') && (t[13] == '-') && (t[18] == '-') && (t[23] == '-') {
			newPath += "_x"
		} else {
			newPath += "_" + t
		}
	}

	result += newPath

	return true, strings.ToLower(result)
}
